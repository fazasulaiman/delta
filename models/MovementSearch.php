<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Movement;

/**
 * MovementSearch represents the model behind the search form of `app\models\Movement`.
 */
class MovementSearch extends Movement
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['movement_date', 'vessel_vessel_id', 'status_code', 'movement_type_code', 'gender_code', 'date_of_birth', 'given_names', 'family_names', 'Nationality', 'visa_type', 'travel_document_no', 'created_by', 'created_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Movement::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'movement_date' => $this->movement_date,
            'date_of_birth' => $this->date_of_birth,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'vessel_vessel_id', $this->vessel_vessel_id])
            ->andFilterWhere(['like', 'status_code', $this->status_code])
            ->andFilterWhere(['like', 'movement_type_code', $this->movement_type_code])
            ->andFilterWhere(['like', 'gender_code', $this->gender_code])
            ->andFilterWhere(['like', 'given_names', $this->given_names])
            ->andFilterWhere(['like', 'family_names', $this->family_names])
            ->andFilterWhere(['like', 'Nationality', $this->Nationality])
            ->andFilterWhere(['like', 'visa_type', $this->visa_type])
            ->andFilterWhere(['like', 'travel_document_no', $this->travel_document_no])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
}
