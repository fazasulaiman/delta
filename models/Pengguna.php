<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengguna".
 *
 * @property int $id A_I
 * @property string $username nip
 * @property string $fullname nama lengkap
 * @property string $created_by
 * @property string $user_id
 * @property string $created_date tgl waktu buat isian
 */
class Pengguna extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pengguna';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'fullname', 'created_by', 'user_id'], 'required'],
            [['created_date'], 'safe'],
            [['username', 'fullname'], 'string', 'max' => 40],
            [['created_by', 'user_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'fullname' => 'Fullname',
            'created_by' => 'Created By',
            'user_id' => 'User ID',
            'created_date' => 'Created Date',
        ];
    }
}
