<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property string $file_name
 * @property string $path
 * @property string $created_on
 * @property string $database
 * @property string $report
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name', 'path'], 'required'],
            [['created_on'], 'safe'],
            [['database', 'report'], 'string'],
            [['file_name'], 'string', 'max' => 40],
            [['path'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'path' => 'Path',
            'created_on' => 'Created On',
            'database' => 'Database',
            'report' => 'Report',
        ];
    }

     public function download(){

        return '<a href="'.$this->path.'"><span class="glyphicon glyphicon-download-alt"></span></a>   <a href="/movement/import?id='.$this->id.'" title="Insert DB" aria-label="Insert DB" data-method="post"><span class="glyphicon glyphicon-hdd"></span></a>  <a href="/movement/remove?id='.$this->id.'" title="Delete" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?" data-method="post"><span class="glyphicon glyphicon-trash"></span></a>';
    }
}
