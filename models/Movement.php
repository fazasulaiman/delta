<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movement".
 *
 * @property int $id
 * @property int $id_file
 * @property string $movement_date tgl perlintasan
 * @property string $vessel_vessel_id flight number
 * @property string $status_code r = refer / a = accept
 * @property string $movement_type_code d = departure / a =arrival
 * @property string $gender_code m = male/ f= female
 * @property string $date_of_birth
 * @property string $given_names
 * @property string $family_names
 * @property string $Nationality
 * @property string $travel_document_no
 * @property string $visa_type tipe visa
 * @property string $created_by
 * @property string $created_on tgl dan waktu
 */
class Movement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_file', 'movement_date', 'vessel_vessel_id', 'status_code', 'movement_type_code', 'gender_code', 'date_of_birth', 'travel_document_no', 'created_by'], 'required'],
            [['id_file'], 'integer'],
            [['movement_date', 'date_of_birth', 'created_on'], 'safe'],
            [['status_code', 'movement_type_code', 'gender_code'], 'string'],
            [['vessel_vessel_id'], 'string', 'max' => 50],
            [['given_names', 'family_names', 'Nationality', 'travel_document_no', 'visa_type', 'created_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_file' => 'Id File',
            'movement_date' => 'Movement Date',
            'vessel_vessel_id' => 'Vessel Vessel ID',
            'status_code' => 'Status Code',
            'movement_type_code' => 'Movement Type Code',
            'gender_code' => 'Gender Code',
            'date_of_birth' => 'Date Of Birth',
            'given_names' => 'Given Names',
            'family_names' => 'Family Names',
            'Nationality' => 'Nationality',
            'travel_document_no' => 'Travel Document No',
            'visa_type' => 'Visa Type',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
        ];
    }
}
