<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\components\Jadwal;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($date = '2018-10-01')
    {
        $result = Jadwal::first($date);
        echo $result;

    }
    public function actionGenerate($date = '2018-10-01')
    {
        $result = Jadwal::generate($date);
        var_dump($result);

    }
    public function actionCurl(){
       
                //create array of data to be posted
        $post_data['loginUsername'] = 'FAZA+AHMAD+SULA';
        $post_data['loginPassword'] = 'katasandi94';
        $post_data['usernip']= '199410272017121001';

        //traverse array and prepare data for posting (key1=value1)
        foreach ( $post_data as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }

        //create the final string to be posted using implode()
        $post_string = implode ('&', $post_items);
        $curl_connection = curl_init();
          

        //set options
curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
curl_setopt($curl_connection, CURLOPT_URL, "https://simpeg.kemenkumham.go.id/devp/siap/signin.php");
        curl_setopt($curl_connection, CURLOPT_HEADER, true);
        curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, false);
         curl_setopt($curl_connection, CURLINFO_HEADER_OUT, true);
        
curl_setopt($curl_connection, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
curl_setopt($curl_connection, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');

        //set data to be posted
        curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);

        //perform our request
        $result = curl_exec($curl_connection);
        var_dump($result);
        echo "string";
        var_dump(curl_getinfo($curl_connection, CURLINFO_HEADER_OUT));

        if (preg_match('~Location: (.*)~i', $result, $match)) {
   $location = trim($match[1]);
}


     
curl_setopt($curl_connection, CURLOPT_URL, 'https://simpeg.kemenkumham.go.id/devp/siap/index.php/pegawai/getLokasiKerja');
curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
curl_setopt ($curl_connection, CURLOPT_POST, 1); 
curl_setopt ($curl_connection, CURLOPT_POSTFIELDS, 'id=199410271'); 
curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, false);


$data = curl_exec($curl_connection);
var_dump($data);


        curl_close($curl_connection);


        
    }

    public function actionHome(){

        $ch = curl_init();

        // set url login
        curl_setopt($ch, CURLOPT_URL, "https://simpeg.kemenkumham.go.id/devp/siap/index_new.php?l=logout");

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        var_dump($output);

        // close curl resource to free up system resources
        curl_close($ch); 

    }

}
