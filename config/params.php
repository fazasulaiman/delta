<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'pattern' => ['pagi','pagi','siang','siang','malam','malam','off','off'],
];
