<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MovementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'movement_date',
            'vessel_vessel_id',
            'status_code',
            'movement_type_code',
            'gender_code',
            'date_of_birth',
            'given_names',
            'family_names',
            'Nationality',
            'visa_type',
            'travel_document_no',
            'created_by',
            'created_on'
        ],
    ]); ?>

