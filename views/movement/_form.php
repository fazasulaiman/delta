<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Movement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'movement_date')->textInput() ?>

    <?= $form->field($model, 'vessel_vessel_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_code')->dropDownList([ 'A' => 'A', 'R' => 'R', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'movement_type_code')->dropDownList([ 'D' => 'D', 'A' => 'A', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'gender_code')->dropDownList([ 'M' => 'M', 'F' => 'F', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'date_of_birth')->textInput() ?>

    <?= $form->field($model, 'given_names')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'family_names')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nationality')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'visa_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'travel_document_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_on')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
