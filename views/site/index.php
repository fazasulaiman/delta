<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use kartik\date\DatePicker;


$this->title = 'Kalender Kerja Delta 2 Online';
Yii::$app->view->registerMetaTag([
      'name' => 'description',
      'content' => 'generate Kalender kerja delta 2 sampai 100 tahun kemudian'
    ]); 
?>
<style>
.pagi {
  height: 50px;
  width: 50px;
  background-color: #feffb3;
}
.siang {
  height: 50px;
  width: 50px;
  background-color: #ffe0b2;
}
.malam {
  height: 50px;
  width: 50px;
  background-color: #bbbaca;
}
.off {
  height: 50px;
  width: 50px;
  background-color: #ffb3b3;
}
.legend { list-style: none; }
.legend li { float: left; margin-right: 10px; }
.legend span { border: 1px solid #ccc; float: left; width: 12px; height: 12px; margin: 2px; }
/* your colors */
.legend .superawesome { background-color: #feffb3; }
.legend .awesome { background-color: #ffe0b2; }
.legend .kindaawesome { background-color: #bbbaca; }
.legend .notawesome { background-color: #ffb3b3; }

</style>
<h1 class="text-center font-weight-bold">Kalender Kerja Delta 2</h1>
<div class="site-index">
    <div class="row" style=" padding-top: 50px;">
      <label class="control-label">Pilih Bulan kerja</label>
      <?= DatePicker::widget([
    'name' => 'issue_date', 
    'type' => DatePicker::TYPE_BUTTON,
    'id' => 'issue_date',
    'pluginOptions' => [
        'format' => 'yyyy-mm-01',
        'startView' => 'months',
        'minViewMode' => 'months',
        'autoclose' => 'true',
    ]
    ]);?>
  
  </div>
<ul class="legend">
    <li><span class="superawesome"></span> Kedatangan</li>
    <!-- <li><span class="awesome"></span> Siang</li> -->
    <li><span class="kindaawesome"></span> Keberangkatan</li>
    <li><span class="notawesome"></span> Off</li>
</ul>

<?= \talma\widgets\FullCalendar::widget([
    'googleCalendar' => false,  // If the plugin displays a Google Calendar. Default false

    
    'loading' => 'Memuat...', // Text for loading alert. Default 'Loading...'
    'config' => [
        // put your options and callbacks here
        // see http://arshaw.com/fullcalendar/docs/
        // optional, if empty get app language
        'customButtons' => [
        
        ],
      'fixedWeekCount' => false,
      'header' => [
        'right' => 'today prev,next '
      ],
      
      'lang' => 'id',
      'events' => $events,
      'dayClick' => function($date, $jsEvent, $view) {
            alert("Day Clicked");
        },
    ],
    
]); ?>
<style type="text/css">
    .fc-today
{
  background-color:inherit !important;
}
</style>


</div>
<?php
$url =  Url::toRoute('site/jadwal');

$script = <<< JS
    
    function Gjadwal(tgl){

        var data;
        $.get( "$url",{date: moment(tgl).format('YYYY-MM-01')}).done(function(events){
            events = JSON.parse(events);
            $('#w0').fullCalendar('addEventSource', events);
        }) 
         
    }
   
    $(document).on('click','.fc-prev-button,.fc-next-button', function () {
        $('#w0').fullCalendar('removeEvents');
        var tgl = $('#w0').fullCalendar('getDate').toDate();
        tgl.setMonth(tgl.getMonth());
        Gjadwal(tgl);
    })
    $(".fc-today-button").click(function() {
        $('#w0').fullCalendar('removeEvents');
        var tgl = $('#w0').fullCalendar('getDate').toDate();
        tgl.setMonth(tgl.getMonth());
        Gjadwal(tgl);
    });
    $("#issue_date").change(function() {
        $('#w0').fullCalendar('removeEvents');
        var tgl = $(this).val();
        $.get( "$url",{date: tgl}).done(function(events){
            events = JSON.parse(events);
            $('#w0').fullCalendar('addEventSource', events);
            date = moment(tgl, "YYYY-MM-DD");
            $("#w0").fullCalendar( 'gotoDate', date );
        }) 
        
    });
     $('#w0').fullCalendar({
  dayClick: function(date, jsEvent, view, resourceObj) {

    alert('Date: ' + date.format());
    alert('Resource ID: ' + resourceObj.id);

  }
});

JS;
$this->registerJs($script);
?>
