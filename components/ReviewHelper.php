<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\Exception;
use common\models\Review;

class ReviewHelper extends Component {


    public function starrating($product){



      $TotalNilai = 0;
      $TotalKoresponden = 0;

      $query = (new \yii\db\Query())
              ->select(['rating','count(*) as jumlah'])
              ->from('review')
              ->where(['product_id' => $product])
              ->groupBy(['rating'])
              ->all();

      if (empty($query)) {
         $rating = array(
          'TotalNilai' =>$TotalNilai,
          'TotalKoresponden' => $TotalKoresponden,
          'hasil' => 0,
       );
       return $rating; 
      }
              

     /* if(empty($query)){
        $rating = array(
          'TotalNilai' =>0,
          'TotalKoresponden' => 0,
          'hasil' => 0,
       );
       return $rating; 
      }*/
       
     

      
      foreach ($query as $each) {
          
          $TotalNilai += $each['rating'] * $each ['jumlah'];
          $TotalKoresponden += $each ['jumlah'];
          $rating['rating-'.$each['rating']] = $each['jumlah'];
      }
      if ($TotalKoresponden == 0.0) {
          $hasil = 0;
          
      } else {
         $hasil = round($TotalNilai/$TotalKoresponden,1);
      }
      

      $rating = array(
          'rating' => $rating,
          'TotalNilai' => $TotalNilai,
          'TotalKoresponden' => $TotalKoresponden,
          'hasil' => $hasil,
       );

      return $rating;
    }

}