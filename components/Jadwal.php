<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\Exception;

class Jadwal extends Component {

  public static $pattern;
  public static $section;
  public static $fraction;


    public  function first ($date)
    {
      
      try {
          
          $star = new \DateTime('2018-11-01');
          $date = new \DateTime($date);
          
          $interval = $date->diff($star)->days;
          self::$fraction = self::section($interval);
          $arr = $interval % 8;
          self::$pattern = $arr;
          return \Yii::$app->params['pattern'][$arr];
      } catch (Exception $e) {
      
        Yii::error($e);
        return false;  
      }

    }
    public function section($interval){

      $n=$interval/8;
      if (ceil($n) % 2 == 0) {
         self::$section = "Keberangkatan";
      }else{
        self::$section= "Kedatangan";
      }

      $whole = floor($n);
      $fraction = $n-$whole;

      return $fraction;
      

    }
    public function generate($date){

    $yearmonth = explode('-', $date); 


    $result = array();



    self::first($date);
    $fraction = self::$fraction;
    
    $arr = self::$pattern;
  
    $d = cal_days_in_month(CAL_GREGORIAN,$yearmonth[1],$yearmonth[0]);

     for ($i=1; $i <= $d ; $i++) { 

        if ($arr>7) {
            $arr = 0 ;
        }
        if ($fraction == 1) {

          if (self::$section == "Kedatangan") {
            self::$section = "Keberangkatan";
          }else{
            self::$section = "Kedatangan";
          }

          $fraction = $fraction -1;
        }


      $result[$i] = \Yii::$app->params['pattern'][$arr].'|'.self::$section;


        //$result[$i][\Yii::$app->params['pattern'][$arr]];

        $fraction = $fraction + 0.125;
        $arr++;
     }
     return $result;
    }

    public function color($shift){
      /*switch ($shift) {
          case "pagi":
              $color = '#ffff00';
              break;
          case "siang":
               $color = '#ff9900';
              break;
          case "malam":
               $color = ' #19194d';
              break;
          default:
              $color = 'red';
      }*/
      $var = explode('|', $shift);
      if ($var[0]=='off') {
        
        $color = "red";
      }else{

        switch ($var[1]) {
          case "Kedatangan":
              $color = '#ffff00';
              break;
          case "Keberangkatan":
               $color = '#19194d';
              break;
        }
      }
      
      return $color;


    }
    

}