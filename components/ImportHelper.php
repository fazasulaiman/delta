<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\Exception;

class ImportHelper extends Component {


   public function Dob($date){

    $explode = explode('/', $date);

    $glue = array($explode[2],$explode[0],$explode[1] );
    $date = implode('-', $glue);


    return $date;

   }

   public function convertDateTime($EXCEL_DATE){

    $UNIX_DATE = ($EXCEL_DATE - 25569) * 86400;
    $EXCEL_DATE = 25569 + ($UNIX_DATE / 86400);
    $UNIX_DATE = ($EXCEL_DATE - 25569) * 86400;
    return gmdate("Y-m-d H:i:s", $UNIX_DATE);

   }

}
