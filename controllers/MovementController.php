<?php

namespace app\controllers;

use Yii;
use app\models\Movement;
use app\models\MovementSearch;
use app\models\UploadForm;
use app\models\FileSearch;
use app\models\File;
use app\components\ImportHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;


/**
 * MovementController implements the CRUD actions for Movement model.
 */
class MovementController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Movement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MovementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Movement model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Movement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Movement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Movement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Movement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Movement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Movement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Movement::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionUpload()
    {
        $searchModel = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new UploadForm();
        $files = new FileSearch();
        $path = 'uploads/movement/';

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            
            if ($model->file && $model->validate()) {  
                         
                $model->file->saveAs($path. $model->file->baseName . '.' . $model->file->extension);
                $files->file_name = $model->file->baseName . '.' . $model->file->extension;
                $files->path = '../'.$path . $model->file->baseName . '.' . $model->file->extension;
                $files->save();
                Yii::$app->getSession()->setFlash('success','Success');
            }
            else {
                // validation failed: $errors is an array containing error messages
                $errors = $model->errors;
                Yii::$app->getSession()->setFlash('success',$errors);
              
            }
        }
        
         return $this->render('upload', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionRemove($id)
    {
       
        $data = File::findOne($id);
        Movement::deleteAll(['id_file' => $id]);
        unlink(getcwd().'/uploads/movement/'.$data->file_name);
        $data->delete();

        Yii::$app->getSession()->setFlash('success','Success');

        return $this->redirect(['upload']);
    }
    public function actionImport($id)
    {
        ini_set('memory_limit', '-1');
        $data = File::findOne($id);
        $file = 'uploads/movement/'. $data->file_name;
        
        //$inputFileType = \PHPExcel_IOFactory::identify($file); otomatis
        $objReader = \PHPExcel_IOFactory::createReader('Excel5');
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($file);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $formulaValue = $objPHPExcel->getActiveSheet();
        $baseRow = 5;
        while(!empty($sheetData[$baseRow]['B'])){
           
          
          
            $model = new Movement();
            $model->id_file = $id;
            $model->movement_date = \PHPExcel_Style_NumberFormat::toFormattedString($formulaValue->getCell('A'.$baseRow)->getCalculatedValue(), 'YYYY-MM-DD H:i:s');
            $model->vessel_vessel_id = $formulaValue->getCell('B'.$baseRow)->getCalculatedValue();
            $model->status_code = $formulaValue->getCell('G'.$baseRow)->getCalculatedValue();
            $model->movement_type_code = $formulaValue->getCell('H'.$baseRow)->getCalculatedValue();
            $model->gender_code = $formulaValue->getCell('J'.$baseRow)->getCalculatedValue();
            $model->date_of_birth = \PHPExcel_Style_NumberFormat::toFormattedString($formulaValue->getCell('K'.$baseRow)->getCalculatedValue(), 'YYYY-MM-DD');
            $model->given_names = $formulaValue->getCell('D'.$baseRow)->getCalculatedValue();
            $model->family_names = $formulaValue->getCell('E'.$baseRow)->getCalculatedValue();
            $model->Nationality = $formulaValue->getCell('L'.$baseRow)->getCalculatedValue();
            $model->travel_document_no = $formulaValue->getCell('C'.$baseRow)->getCalculatedValue();
            $model->visa_type = $formulaValue->getCell('F'.$baseRow)->getCalculatedValue();
            $model->created_by = $formulaValue->getCell('M'.$baseRow)->getCalculatedValue();
            $model->created_on = \PHPExcel_Style_NumberFormat::toFormattedString($formulaValue->getCell('N'.$baseRow)->getCalculatedValue(), 'YYYY-MM-DD H:i:s');
            $model->save();
            $baseRow++;
        }

        $data->database = 'yes';
        $data->save(); 
        Yii::$app->getSession()->setFlash('success','Success');

        return $this->redirect(['upload']);
    }
}
