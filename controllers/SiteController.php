<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
 use app\models\EventCalander;
 use app\components\Jadwal;
 use thiagoalessio\TesseractOCR\TesseractOCR;
 

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['jadwal'],
                        'allow' => true,
                    ],
                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
     
      $model = new Jadwal();
      $jadwal = $model->generate(date('Y-m-01'));
     
      $events = array();
      

      foreach ($jadwal as $key => $item) {
        $Event = new EventCalander();
        $Event->id = date('Y-m-').sprintf("%02d", $key);
        
        $Event->start = date($Event->id);
        $Event->color=   $model->color($item);
        $Event->rendering= 'background';
        $Event->allDay= true;
        $Event->title = $item;
       
        $events[] = $Event;
       
      }
        return $this->render('index',['events' => $events ]);
    }
    public function actionJadwal($date)
    {
        $model = new Jadwal();
        $jadwal = $model->generate($date);
        foreach ($jadwal as $key => $item) {
            $Event = new EventCalander();
            $Event->id = substr($date, 0, -2).sprintf("%02d", $key);
            $Event->title = $item;
            $Event->start = date($Event->id);
            $Event->color=  $model->color($item);
            $Event->rendering= 'background';
            $Event->allDay= true;
         
            $events[] = $Event;

           
          }

       return json_encode($events);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
